import { Mongoose } from "mongoose";

const mongoose = require('mongoose');
const MONGODB_RETRY_TIME = process.env.MONGODB_RETRY_TIME || 2000;
const MONGO_PORT = 27017;
const mongoUri = process.env.MONGO_URI


export default class MongooseManager {

    constructor() {
        if (!mongoUri) {
            throw new Error("Not mongo uri finded");
        }
    }

    async connectMongo() {
        try {
            mongoose.connect(mongoUri, {
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                user: process.env.MONGO_DB_USER,
                pass: process.env.MONGO_DB_PASS,
                auth:{
                    "authSource": "admin"
                }
            });
            
            mongoose.connection.on('error', (error: Error) => {
                console.log('DB connection failed. Will try again.');
                console.log(error)
            });
    
            mongoose.connection.on('connected', function () {
                console.log("\nMONGO CONNECTED: " + mongoUri)
            });
            
            mongoose.connection.on('disconnected', function () {
                console.log('Desconectado de la base de datos')
            })
            
            await mongoose.connection.once("open", function() {
                console.log("MongoDB database connection established successfully\n======================");
            })
            
            process.on('SIGINT', function() {
                mongoose.connection.close(function () {
                    console.log('Desconectado de la base de datos al terminar la app')
                    process.exit(0)
                })
            })
        } catch (error) {
            console.log("MONGO ERROR")
            console.log(error)
        }
    }

    getMongoose(): Mongoose {
        return mongoose;
    }
}