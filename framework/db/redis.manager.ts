import { PaginationResponseInterface } from "../utils/pagination.interface";

const redis = require('redis');
const { promisify } = require('util');
const REDIS_URL = process.env.REDIS_URL || "redis://localhost:6379"
const REDIS_HOST = process.env.REDIS_HOST || undefined
const REDIS_HOST_PUBSUB = process.env.REDIS_HOST_PUBSUB
const REDIS_PASS = process.env.REDIS_PASS
const REDIS_CACHE_TIMEOUT = Number(process.env.REDIS_CACHE_TIMEOUT) || 120

import { RedisPubSub } from 'graphql-redis-subscriptions';
export class RedisManager {
    private static instance: RedisManager;

    client = REDIS_HOST ? redis.createClient( { url: REDIS_HOST} ) : undefined
    clientPublisher? : any
    clientSubscriber? : any
    redisPubsub?: RedisPubSub
    timeout: number = Number(REDIS_CACHE_TIMEOUT)

    constructor() {
        if (!REDIS_HOST) {
            console.log(`\n REDIS: Not need connection to redis database. Maybe need to redis pubsub.\n======================`)
            return
        }

        if (!this.client) {
            this.client = redis.createClient( { url: REDIS_HOST} )
        }

        this.client.on("error", (error: Error) => {
            console.error(`\n ==>>> ERROR to connect Redis: ${error}\n======================`);
        });

        this.client.on('ready',function() {
            console.log(`\nRedis connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds\n======================`);
        });
    }

    public static getInstance(): RedisManager {
        if (!RedisManager.instance) {
            RedisManager.instance = new RedisManager();
        }

        return RedisManager.instance;
    }

    public configureRedisPubSub() {
        this.clientPublisher = this.getRedisClientPublisher()
        this.clientSubscriber = this.getRedisClientSubscriber()
        this.redisPubsub = new RedisPubSub({
            publisher: this.clientPublisher,
            subscriber: this.clientSubscriber
        })
    }

    public getRedisClientPublisher(): RedisManager {
        if (!REDIS_HOST_PUBSUB || REDIS_HOST_PUBSUB === "") {
            throw new Error("Can not connect to redis pubsub because we don't have configuration params")
        }
        if (this.clientPublisher !== undefined) {
            return this.clientPublisher
        }

        const redisClient = redis.createClient( { url: REDIS_HOST_PUBSUB} )

        redisClient.on("error", (error: Error) => {
            console.error(`\n ==>>> ERROR to connect Redis as Publisher: ${error}\N======================`);
        });

        redisClient.on('ready',function() {
            console.log(`\nRedis Publisher (${REDIS_HOST_PUBSUB}) connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds\n======================`);
        });

        return redisClient
    }

    public getRedisClientSubscriber(): RedisManager {
        if (!REDIS_HOST_PUBSUB || REDIS_HOST_PUBSUB === "") {
            throw new Error("Can not connect to redis pubsub because we don't have configuration params")
        }
        if (this.clientSubscriber !== undefined) {
            return this.clientSubscriber
        }

        const redisClient = redis.createClient( { url: REDIS_HOST_PUBSUB} );

        redisClient.on("error", (error: Error) => {
            console.error(`\n ==>>> ERROR to connect Redis as Subscriber: ${error}\N======================`);
        });

        redisClient.on('ready',function() {
            console.log(`\nRedis Subscriber (${REDIS_HOST_PUBSUB}) connection is ready. Cache timeout is ${REDIS_CACHE_TIMEOUT} seconds\n======================`);
        });

        return redisClient
    }

    // async saveArray(key:string, array: any[], pagination: PaginationResponseInterface | undefined = undefined, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
    async saveArray(key:string, array: any[], pagination: PaginationResponseInterface | undefined = undefined, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
        try {
            await this.delAsync(key)
            for (let i = 0; i < array.length; i++) {
                const item = array[i]
                const objectKey = `${key.split(":")[0]}:${item._id}`
                await this.hmsetAsync(objectKey, item)
                await this.expireAsync(objectKey, expire)
                await this.lpushAsync(key, objectKey)
            }
            await this.expireAsync(key, expire)
            if (pagination) {
                await this.hmsetAsync(`${key}:pagination`, pagination)
                await this.expireAsync(`${key}:pagination`, expire)
            }
            return Promise.resolve(true)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async getArray<T>(key: string): Promise<{items: T[], pagination: PaginationResponseInterface}> {
        const keys = await this.lrangeAsync(key, 0, -1) // 0, -1 => all items
        // const sort = await this.client.sort(key)
        // sort listLogs by nosort get *->_id get *->serviceProvider
        const items :any[] = []
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const log = await this.hgetallAsync(key)
            items.push(log)
        }
        const pagination = await this.hgetallAsync(`${key}:pagination`)
        return {items: items, pagination: pagination}
    }

    async saveObject(key: string, item: any, expire: number = REDIS_CACHE_TIMEOUT): Promise<Boolean> {
        try {
            const set = await this.hmsetAsync(`${key}:${item._id}`, item)
            await this.expireAsync(`${key}:${item._id}`, expire)
            if (set)  {
                return Promise.resolve(true)
            }
            return Promise.resolve(false)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async saveObjectAndArray(key: string, item: any): Promise<Boolean> {
        try {
            const itemSaved = await this.hmsetAsync(`${key}:${item._id}`, item)
            if (itemSaved) {
                const objectKey = `${key}:${item._id}`
                const listKey = key+":list"
                await this.lremAsync(listKey, 1, objectKey)
                await this.lpushAsync(listKey, objectKey)
                return Promise.resolve(true)
            }
            return Promise.resolve(false)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async getObject<T>(key: string): Promise<T> {
        const item = await this.hgetallAsync(key)
        return item
    }

    async deleteObjectAndUpdateArray(objectKey: string, key: string) {
        try {
            await this.delAsync(objectKey)
            await this.lremAsync(key+":list", 1, objectKey)
        } catch (error) {
            throw new Error("Delete object error.");
        }
    }

    async clearCache(key: string) {
        try {
            // Delete simple key
            const delKey = await this.delAsync(key)
            if (!delKey) {
                // Delete * list keys
                const keys = await this.keysAsync(key)
                if (keys) {
                    for (let i = 0; i < keys.length; i++) {
                        const key = keys[i];
                        await this.delAsync(key)
                    }
                }
            }
        } catch (error) {
            throw new Error("Clear caché error.");
        }
    }

    getListCacheKey(path: string, query: string[]) {
        let key = path + ":list:"
        query.forEach(k => {
            key = key + "_" + k
        })
        return key
    }

    getObjectCacheKey(path: string, id: string) {
        let key = path + ":" + id
        return key
    }

    expireAsync = this.client ? promisify(this.client.expire).bind(this.client) : undefined
    getAsync    = this.client ? promisify(this.client.get).bind(this.client) : undefined
    setAsync    = this.client ? promisify(this.client.set).bind(this.client) : undefined
    delAsync    = this.client ? promisify(this.client.del).bind(this.client) : undefined
    hgetallAsync= this.client ? promisify(this.client.hgetall).bind(this.client) : undefined
    hmsetAsync  = this.client ? promisify(this.client.hmset).bind(this.client) : undefined
    setexAsync  = this.client ? promisify(this.client.setex).bind(this.client) : undefined
    keysAsync   = this.client ? promisify(this.client.keys).bind(this.client) : undefined
    lpushAsync  = this.client ? promisify(this.client.lpush).bind(this.client) : undefined
    lrangeAsync = this.client ? promisify(this.client.lrange).bind(this.client) : undefined
    lremAsync   = this.client ? promisify(this.client.lrem).bind(this.client) : undefined
}


export const redisManager = RedisManager.getInstance()