import { Identifiable } from "../base/basemodel.interface";
import { ControllerInterface } from "../base/controller.interface";

export interface GraphqlObjectInterface<T extends Identifiable> {
    controller: ControllerInterface<T>
    types: string
    inputs: string
    queries: string
    mutations: string
    subscriptions: string
    query: any
    mutation: any
    subscription: any

    defineTypes(): void
    defineInputs(): void
    defineQueries(): void
    defineMutations(): void
    defineSubscriptions(): void
    defineResolvers(controller: ControllerInterface<T>): void
    getGraphQLValues(): any
}

export interface GraphqlObjectMicroservice {
    path: string
    types: string
    inputs: string
    queries: string
    mutations: string
    subscriptions: string
    query: any
    mutation: any
    subscription: any

    defineTypes(): void
    defineInputs(): void
    defineQueries(): void
    defineMutations(): void
    defineSubscriptions(): void
    defineResolvers(): void
    getGraphQLValues(): any
}

