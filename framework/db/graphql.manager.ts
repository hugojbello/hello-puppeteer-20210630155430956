import { Identifiable } from "../base/basemodel.interface";
import { AuthMiddlewareInterface } from "../utils/jwt.middleware.interface";
import { GraphqlObjectInterface, GraphqlObjectMicroservice } from "./graphql.object.interface";
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { RedisManager } from "./redis.manager";

const ws = require('ws');
const { getGraphQLParams, graphqlHTTP } = require('express-graphql');
const { execute, subscribe } = require('graphql');
const { useServer } = require('graphql-ws/lib/use/ws');
const { makeExecutableSchema, mergeTypeDefs, mergeResolvers } = require('graphql-tools');

export default class GraphQLManager {
    app: any
    graphQLObjects : GraphqlObjectInterface<Identifiable>[]
    graphQLMicroservicesObjects : GraphqlObjectMicroservice[] = []
    authenticationMiddleware: AuthMiddlewareInterface | undefined

    private redisPubsub : RedisPubSub
    private schema: any
    private inputs: string[] = [];
    private types: string[] = [];
    private queries: string[] = [];
    private mutations: string[] = [];
    private subscriptions: string[] = [];
    private resolvers: any[] = []

    constructor(app: any, graphqlObjects: GraphqlObjectInterface<Identifiable>[], graphQLMicroservicesObjects: GraphqlObjectMicroservice[] = [], authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, redisDataBasePublisher: RedisManager, redisDataBaseSubscriber: RedisManager) {
        this.app = app
        this.graphQLObjects = graphqlObjects
        this.graphQLMicroservicesObjects = graphQLMicroservicesObjects
        this.authenticationMiddleware = authenticationMiddleware
        this.redisPubsub = new RedisPubSub({
            publisher: redisDataBasePublisher.client,
            subscriber: redisDataBaseSubscriber.client
        })
    }
    
    getRedisPubSub(): RedisPubSub {
        return this.redisPubsub
    }

    getSchema(): any {
        return this.schema
    }

    getValues() {
        this.graphQLObjects.forEach(graphqlObject => {
            const graphQL = graphqlObject.getGraphQLValues()
            if (graphQL.inputs && graphQL.inputs !== "") {
                this.inputs.push(graphQL.inputs)
            }
            if (graphQL.types && graphQL.types !== "") {
                this.types.push(graphQL.types);
            }
            if (graphQL.queries && graphQL.queries !== "") {
                this.queries.push(graphQL.queries)
            }
            if (graphQL.mutations && graphQL.mutations !== "") {
                this.mutations.push(graphQL.mutations)
            }
            if (graphQL.subscriptions && graphQL.subscriptions !== "") {
                this.subscriptions.push(graphQL.subscriptions)
            }
            if (graphQL.query && graphQL.query.Query) {
                this.resolvers.push(graphQL.query)
            }
            if (graphQL.mutation && graphQL.mutation.Mutation) {
                this.resolvers.push(graphQL.mutation)
            }
            if (graphQL.subscription && graphQL.subscription.Subscription) {
                this.resolvers.push(graphQL.subscription)
            }
        })

        this.graphQLMicroservicesObjects.forEach(graphqlObject => {
            const graphQL = graphqlObject.getGraphQLValues()
            if (graphQL.inputs && graphQL.inputs !== "") {
                this.inputs.push(graphQL.inputs)
            }
            if (graphQL.types && graphQL.types !== "") {
                this.types.push(graphQL.types);
            }
            if (graphQL.queries && graphQL.queries !== "") {
                this.queries.push(graphQL.queries)
            }
            if (graphQL.mutations && graphQL.mutations !== "") {
                this.mutations.push(graphQL.mutations)
            }
            if (graphQL.subscriptions && graphQL.subscriptions !== "") {
                this.subscriptions.push(graphQL.subscriptions)
            }
            if (graphQL.query && graphQL.query.Query) {
                this.resolvers.push(graphQL.query)
            }
            if (graphQL.mutation && graphQL.mutation.Mutation) {
                this.resolvers.push(graphQL.mutation)
            }
            if (graphQL.subscription && graphQL.subscription.Subscription) {
                this.resolvers.push(graphQL.subscription)
            }
        })
    }

    generateGraphQLSchema() {
        this.getValues()

        let typeDefs :any[] = []
        if (this.inputs.length > 0) { typeDefs.push(this.inputs) }
        if (this.types.length > 0) { typeDefs.push(this.types) }
        if (this.queries.length > 0) { typeDefs.push(this.queries) }
        if (this.mutations.length > 0) { typeDefs.push(this.mutations) }
        if (this.subscriptions.length > 0) { typeDefs.push(this.subscriptions) }
        typeDefs = mergeTypeDefs(typeDefs)

        // `
        //     ${ this.types.join("\n")}
        //     ${ this.inputs.length > 0 ? this.inputs.join("\n") : "" }
        //     ${ this.queries.length > 0 ? ("type Query {\n" + this.queries.join("\n") + "},") : "" }
        //     ${ this.mutations.length > 0 ? ("type Mutation {\n" + this.mutations.join("\n") + "},") : "" }
        //     ${ this.subscriptions.length > 0 ? ("type Subscription {\n" + this.subscriptions.join("\n") + "}") : "" }
        // `

        const resolvers = mergeResolvers(this.resolvers)
        console.log("\n GRAPHQL RESOLVERS\n======================")
        console.log(resolvers)
        this.schema = makeExecutableSchema({ typeDefs, resolvers });
    }

    configGraphQLRouteAndServe(prefix: string, port: string) {
        const middlewares : any = []
        if (this.authenticationMiddleware) {
            middlewares.push(this.authenticationMiddleware.authenticate)
        }

        this.app.use(
            `${prefix}/graphql`, middlewares, (req: any, res: any, next: any) => {
              getGraphQLParams(req).then((params: any) => {
                // console.log(params);
              })
          
              graphqlHTTP({
                schema: this.schema,
                graphiql: true,
                context: { 
                    req: req,
                    pubsub: this.redisPubsub
                }
              })(req, res, next)
            }
        );

        
    }

    getWSServer(server: any, prefix: string): void {
        const wsServer = new ws.Server({
            server,
            path: `${prefix}/graphql`,
        });

        return useServer(
            {
                execute,
                subscribe,
                schema: this.schema,
                onConnect: this.onConnect,
                onOperation: this.onOperation,
                onDisconnect: this.onDisconnect,
                context: (ctx: any, msg: any, args: any) => new Promise((resolve, reject) => {
                    args.pubsub = this.redisPubsub
                    resolve(args);
                }),
                onSubscribe: (ctx: any, _msg: any, args: any) => {
                    console.log("on subscribe")
                    console.log(args)
                },
            },
            wsServer,
        );
    }

    onOperation(message: any, params: any, WebSocket: any) {
        console.log('subscription ' + message.payload, params);
        // return Promise.resolve(Object.assign({}, params, { context: message.payload.context }))
    }
    
    onConnect(connectionParams:any, WebSocket: any) {
        console.log('connecting ....')
    }
    
    onDisconnect = function (WebSocket: any) {
        console.log('disconnecting ...')
    }


}