import { PaginationInterface, PaginationResponseInterface } from "../utils/pagination.interface";
import { SearchFieldsInterface } from "../utils/searchField.interface";
import { Identifiable } from "./basemodel.interface";

export interface ManagerInterface<U, T extends Identifiable> {
    database: U
    
    list(searchFields: SearchFieldsInterface | undefined, pagination: PaginationInterface | undefined): Promise<{items: T[], pagination: PaginationResponseInterface | undefined}>
    show(id: string): Promise<T | undefined>
    create(item: T): Promise<T>
    update(id: string, item: T): Promise<T | undefined>
    delete(id: string): Promise<T | undefined>

    // itemsToJSON<T extends T>(): T[] 
}