import { RequestHandler as Middleware } from 'express';
import { Handler } from './handler.model'
import { BaseRoute, Method } from './route.interface';

export class Route implements BaseRoute {
    method: Method;
    path: string;
    middleware: Middleware[];
    handler: Handler;

    constructor(method: Method, path: string, middleware: Middleware[], handler: Handler) {
        this.method = method
        this.path = path
        this.middleware = middleware
        this.handler = handler
    }
}