import { Identifiable } from "./basemodel.interface";
import { Builder } from "./builder.interface";
import { RedisManager } from "../db/redis.manager";
import { BaseRoute } from "./route.interface";
import { AuthMiddlewareInterface } from "../utils/jwt.middleware.interface";
import { JWTManager } from "../utils/jwt.manager";
import { CacheMiddleware } from "../utils/cache.middleware";
export interface DirectorInterface<U, T extends Identifiable> {
    builder: Builder<U, T>;
    database: any
    cacheDB?: RedisManager
    authenticationMiddleware?: AuthMiddlewareInterface
    cacheMiddleware?: CacheMiddleware

    getRoutes(): BaseRoute[] | undefined
    setBuilder(builder: Builder<U, T>): void;
}

export interface DirectorAuthInterface<U, T extends Identifiable> {
    jwtManager?: JWTManager
}

export interface DirectorInterfaceMongo<U, T extends Identifiable> extends DirectorInterface<U, T> {
    buildMongo(): void;
    buildMongoWithCache(): void;
}

export interface DirectorInterfaceFirebase<U, T extends Identifiable> extends DirectorInterface<U, T> {
    buildFirebase(): void;
    buildFirebaseWithCache(): void;
}

export interface DirectorInterfaceRedis<U, T extends Identifiable> extends DirectorInterface<U, T> {
    buildRedis(): void
}