import { RequestHandler as Middleware } from 'express';
import { Handler } from './handler.model'
import { Identifiable } from './basemodel.interface';
import { HandlerInterface } from './handler.interface';
import { AuthMiddlewareInterface } from '../utils/jwt.middleware.interface';
import { CacheMiddleware } from '../utils/cache.middleware';

export type Method =
  | 'get'
  | 'head'
  | 'post'
  | 'put'
  | 'delete'
  | 'connect'
  | 'options'
  | 'trace'
  | 'patch';

export interface BaseRoute {
  method: Method;
  path: string;
  middleware: Middleware[];
  handler: Handler;
}

export interface RoutesInterface<T extends Identifiable> {
  handler: HandlerInterface<T>
  routes: BaseRoute[]
  authenticationMiddleware: AuthMiddlewareInterface | undefined
  cacheMiddleware?: CacheMiddleware

  createRoutes(): void
}