const cryptoFW = require('crypto');

// const cryptkey   = "4USGve92PaYRf6szABhUaKUZFmKbW3aC"
// const iv         = 'kGqZEO6keccAkO4g'

export interface CryptoManagerInterface {
    cryptkey: string
    iv: string
    expires: number | undefined

    encrypt(cleardata: string): Promise<string>
    decrypt(encryptdata: string): Promise<string>
}

export class CryptoManager implements CryptoManagerInterface {
    cryptkey: string
    iv: string
    expires: number | undefined

    constructor(cryptkey: string, iv: string, expires: number | undefined = undefined) {
        this.cryptkey = cryptkey
        this.iv = iv
        this.expires = expires
    }

    encrypt(cleardata: string) : Promise<string> {
       
        let buff = Buffer.from(cleardata, 'base64')
        // cleardata = buff.toString('base64');
        
        var cipher = cryptoFW.createCipheriv('aes-256-cbc', this.cryptkey, this.iv);
        var crypted = cipher.update(cleardata,'utf8','binary');
        crypted += cipher.final('binary');
        if (Buffer.from && Buffer.from !== Uint8Array.from) {
            crypted = Buffer.from(crypted, 'binary').toString('base64');
        } else {
            // if (typeof notNumber === 'number') {
            //     return Promise.reject('The "size" argument must be not of type number.')
            //     // throw new Error('The "size" argument must be not of type number.');
            // }
            crypted = Buffer.from(crypted, 'binary').toString('base64');
        }
        return crypted;
    }

    decrypt(encryptdata: string) : Promise<string> {
        if (encryptdata === null || typeof encryptdata === 'undefined' || encryptdata === '') {
            return Promise.resolve(encryptdata)
        }
        if (Buffer.from && Buffer.from !== Uint8Array.from) {
            encryptdata = Buffer.from(encryptdata, 'base64').toString('binary');
        } else {
            // if (typeof notNumber === 'number') {
            //     return Promise.reject('The "size" argument must be not of type number.')
            //     // throw new Error('The "size" argument must be not of type number.');
            // }
            encryptdata = Buffer.from(encryptdata, 'base64').toString('binary');
        } 
        var decipher = cryptoFW.createDecipheriv('aes-256-cbc', this.cryptkey, this.iv);
        var dec = decipher.update(encryptdata,'binary','utf8');
        dec += decipher.final('utf8');
        
        let buff = Buffer.from(dec, 'base64');
        return Promise.resolve(buff.toString('utf-8'))
      }
    
}