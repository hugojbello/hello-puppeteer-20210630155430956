export type ByDate = {
    startDate: string,
    endDate: string
}

export interface SearchFieldsInterface {
    byDate: {startDate: string, endDate: string}
}