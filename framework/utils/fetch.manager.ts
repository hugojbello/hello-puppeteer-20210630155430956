const fetch = require('node-fetch');

export async function httpRequest(url: string, req: any): Promise<any> {
    try {
        const options : {[key: string]: string}= {}
        options.method = req.method
        options.headers = req.headers
        if (req.method === "POST" || req.method === "PUT") {
            options.body = req.body
        }

        const response = await fetch(url, options);

        if (response && response.status < 300) {
            const data = await response.json();
            return Promise.resolve(data)
        }
        const data = await response.json();
        return Promise.reject(data)
    } catch (error) {
        return Promise.reject(error)
    }
    
}
