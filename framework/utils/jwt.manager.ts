const jwt = require('jsonwebtoken');

export interface JWTManagerInterface {
    secret: string
    expires: number | undefined

    generateAccessToken(credentials: {}): Promise<string>
    verifyToken(token: string): Promise<any>
}

export class JWTManager implements JWTManagerInterface {
    secret: string;
    expires: number | undefined;

    constructor(secret: string, expires: number | undefined = undefined) {
        this.secret = secret
        this.expires = expires
    }

    generateAccessToken(credentials: any): Promise<string> {
        const expires = this.expires ? { expiresIn: this.expires } : {}
        return Promise.resolve(jwt.sign(credentials, this.secret, expires))
    }

    verifyToken(token: string): Promise<any> {
        return jwt.verify(token, this.secret, (error: any, credentials: any) => {
            if (error) return Promise.reject(error)
            return Promise.resolve(credentials)
        })
    }
    
}