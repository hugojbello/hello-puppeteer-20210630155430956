export interface PaginationInterface {
    page: number
    limit: number
}
export class Pagination implements PaginationInterface {
    page: number = 1
    limit: number = 100
    lean: boolean = true

    constructor(page: number = 1, limit: number = 100, lean: boolean = true) {
        this.page = page
        this.limit = limit
        this.lean = lean
    }
}
export interface PaginationResponseInterface {
    totalDocs: number
    limit: number
    totalPages: number
    page: number
    hasPrevPage: boolean
    hasNextPage: boolean
    prevPage: number
    nextPage: number
}

export class PaginationResponse implements PaginationResponseInterface {
    totalDocs: number = 0
    limit: number = 0
    totalPages: number = 0
    page: number = 0
    hasPrevPage: boolean = false
    hasNextPage: boolean = false
    prevPage: number = 0
    nextPage: number = 0

    constructor(total: number, page: number, limit: number) {
        this.totalDocs = total
        this.limit = limit
        this.totalPages = Math.ceil(total / limit)
        this.page = page
        this.hasPrevPage = this.totalPages > 1 && this.page > 1 ? true : false
        this.hasNextPage = this.totalPages > 1 && this.page < this.totalPages ? true : false
        this.prevPage = this.hasPrevPage ? this.page - 1 : this.page
        this.nextPage = this.hasNextPage ? this.page + 1 : this.page
    }
}