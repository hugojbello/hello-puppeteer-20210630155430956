export interface ControllerInterface {
    login(userID: string, email: string, password: string) : Promise<String | undefined>
}

import axios from 'axios'
import { User } from '../models/User/User';
import { LoginScraper } from '../scrapers/LoginScraper';

export class DexcomController implements ControllerInterface {
    async login(username: string, password: string): Promise<any | undefined> {
        try {
            const user = { username: username, password: password } as User
            const loginScrapper = new LoginScraper("https://clarity.dexcom.eu/users/auth/dexcom_sts")
            const userCredentials = await loginScrapper.scrapTokenAndUserId(user)

            if (!userCredentials) {
                return Promise.reject(new Error("Can not find token"))
            }
            return Promise.resolve(userCredentials)

        } catch (error) {
            console.error(error);
            return Promise.reject(error)
        }
    }

    async data(subject: string, token: string, dateStart: string, dateEnd: string = new Date().toISOString().slice(0,10)) : Promise<any> {
        try {
            const analysis_session = await this.analysisSession(subject, token)

            const dateRange = '["'+dateStart + '/' + dateEnd + '"]'
            console.log(dateRange)
            const body = '{"localDateTimeInterval":' + dateRange + '}'

            const response = await axios({
                method: 'post',
                url: `https://clarity.dexcom.eu/api/subject/${subject}/analysis_session/${analysis_session}/data`,
                data: body,
                headers: {
                    "Accept": "application/json, text/javascript, */*; q=0.01",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
                    "Access-Token": token,
                    "Cache-Control": "no-cache",
                    "Connection": "keep-alive",
                    "Content-Type": "application/json; charset=utf-8",
                    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
                    //X-CSRF-Token: XFGmXwj2/Er5ppLReZiD1/pgYoTUk+dUqSkPIDNX5rfG1qG4sN3zrfA9z6x8jbiYHcBxFIOTKNuJaY+hGgeAsA==
                    //X-Requested-With: XMLHttpRequest
                }
            })
            if (!response || !response.data) {
                return Promise.reject("Can not find analysis session")
            }
            return Promise.resolve(response.data)
        } catch (error) {
            // console.error(error.to);
            return Promise.reject(error)
        }
    }

    async analysisSession(subject: String, token: string) : Promise<String | undefined> {
        try {
            const analysis_session = await axios({
                method: 'post',
                url: `https://clarity.dexcom.eu/api/subject/${subject}/analysis_session`,
                headers: {
                    "Accept": "application/json, text/javascript, */*; q=0.01",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
                    "Access-Token": token,
                    "Cache-Control": "no-cache",
                    "Connection": "keep-alive",
                    "Content-Length": 0,
                    "Content-Type": "application/json; charset=utf-8",
                    "Host": "clarity.dexcom.eu",
                    "Origin": "https://clarity.dexcom.eu",
                    "Pragma": "no-cache",
                    "Referer": "https://clarity.dexcom.eu/",
                    "Sec-Fetch-Dest": "empty",
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-origin",
                    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
                    //X-CSRF-Token: XFGmXwj2/Er5ppLReZiD1/pgYoTUk+dUqSkPIDNX5rfG1qG4sN3zrfA9z6x8jbiYHcBxFIOTKNuJaY+hGgeAsA==
                    //X-Requested-With: XMLHttpRequest
                }
            })
            if (!analysis_session || !analysis_session.data || !analysis_session.data.analysisSessionId) {
                return Promise.reject("Can not find analysis session")
            }
            return Promise.resolve(analysis_session.data.analysisSessionId)
        } catch (error) {
            console.error(error);
        }
    }
}