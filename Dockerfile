FROM node:14-alpine

# Installs latest Chromium (89) package.
RUN apk add --no-cache \
      chromium \
      nss \
      freetype \
      harfbuzz \
      ca-certificates \
      ttf-freefont \
      nodejs \
      yarn

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser
    
# set app basepath
ENV HOME=/usr/src/app

WORKDIR $HOME
# add app dependencies
COPY package.json ./
COPY package-lock.json ./

# change workgin dir and install deps in quiet mode
# WORKDIR $HOME/node
RUN npm ci -q

# copy all app files
COPY . .

# compile typescript and build all production stuff
RUN npm run build

# remove dev dependencies and files that are not needed in production
RUN npm prune --production

EXPOSE 8080

CMD ["node", "./build/server.js"]

# # start new image for lower size
# FROM node:14-alpine

# # create use with no permissions
# RUN addgroup -g 101 -S app && adduser -u 100 -S -G app -s /bin/false app

# # set app basepath
# ENV HOME=/home/app

# # copy production complied node app to the new image
# COPY --from=build $HOME/node/ $HOME/node/
# RUN chown -R app:app $HOME/*

# # run app with low permissions level user
# USER app
# WORKDIR $HOME/node

# EXPOSE 8080

# CMD ["node", "./build/server.js"]