import { BaseRoute } from "../../framework/base/route.interface";
import { RedisManager } from "../../framework/db/redis.manager";
import MongooseManager from "../../framework/db/mongoose.manager";
import { CryptoManager } from "../../framework/utils/crypto.manager";
import { AuthMiddlewareInterface } from "../../framework/utils/jwt.middleware.interface";
import { CacheMiddleware } from "../../framework/utils/cache.middleware";
import { DexcomDBBuilder } from "./dexcom.builder";
import { DexcomController } from "../../controllers/dexcom.controller";
import { DexcomHandler } from "../../handlers/dexcom.handler";
import { DexcomRoutes } from "../../routes/dexcom.routes";

export class DexcomDirector {
    builder: DexcomDBBuilder = new DexcomDBBuilder()
    database?: MongooseManager
    cacheDB?: RedisManager
    authenticationMiddleware?: AuthMiddlewareInterface
    cacheMiddleware?: CacheMiddleware

    constructor(database?: MongooseManager, cacheDB?: RedisManager | undefined, authenticationMiddleware?: AuthMiddlewareInterface, cacheMiddleware?: CacheMiddleware) {
        this.database = database
        this.cacheDB = cacheDB
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
    }

    getRoutes(): BaseRoute[] {
        return this.builder.getRoutes() ? this.builder.getRoutes()!.routes : []
    }

    public setBuilder(builder: DexcomDBBuilder): void {
        this.builder = builder;
    }

    public buildMongo(cache: string = "False"): void {
        // const dexcomDataBase = new MsgManagerMongo(this.database)
        const dexcomController = new DexcomController()
        const dexcomHandler = new DexcomHandler(dexcomController)
        const dexcomRoutes = new DexcomRoutes(dexcomHandler, this.authenticationMiddleware, this.cacheMiddleware)
        this.builder
            .setController(dexcomController)
            .setHandler(dexcomHandler)
            .setRoutes(dexcomRoutes)
            // .setDatabase(logDataBase)
            

        let encrypt = "false"
        if (process.env.CRYPTKEY && process.env.CRYPTIV) {
            const msgCrypt = new CryptoManager(process.env.CRYPTKEY, process.env.CRYPTIV)
            this.builder.setEncryption(msgCrypt)
            encrypt = "true"
        }

        console.log("- Msg; DataBase: MongoDB; RedisCache: " + cache + "; Encryption: " + encrypt)
    }

    public buildMongoWithCache(): void {
        if (this.cacheDB) {
            this.buildMongo("True")
            this.builder.setCacheDB(this.cacheDB)
        } else {
            throw new Error("Can no create Mongo with cache because there are not cache db assigned.");
        }
    }

}