import { ManagerInterface } from "../../framework/base/manager.interface";
import { RedisManager } from "../../framework/db/redis.manager";
import MongooseManager from "../../framework/db/mongoose.manager";
import { CryptoManagerInterface } from "../../framework/utils/crypto.manager";
import { DexcomRoutes } from "../../routes/dexcom.routes";
import { DexcomHandler } from "../../handlers/dexcom.handler";
import { DexcomController } from "../../controllers/dexcom.controller";

export class DexcomDBBuilder  {
    routes?: DexcomRoutes
    handler?: DexcomHandler
    controller?: DexcomController
    cacheDataBase?: RedisManager
    cache: Boolean = false;
    encryptionManager?: CryptoManagerInterface; 

    setRoutes(routes: DexcomRoutes): DexcomDBBuilder {
        this.routes = routes
        return this
    }
    setHandler(handler: DexcomHandler): DexcomDBBuilder {
        this.handler = handler
        if (this.routes) {
            this.routes.handler = handler
        }
        return this
    }
    setController(controller: DexcomController): DexcomDBBuilder {
        this.controller = controller
        if (this.handler) {
            this.handler.controller = controller
        }
        return this
    }
    setCacheDB(redisManager: RedisManager | undefined): DexcomDBBuilder {
        this.cacheDataBase = redisManager
        return this
    }
    setCache(cache: Boolean): DexcomDBBuilder {
        this.cache = cache
        return this
    }
    setEncryption(encryptionManager: CryptoManagerInterface): DexcomDBBuilder {
        this.encryptionManager = encryptionManager
        if (this.controller) {
            // this.controller.encryptionManager = encryptionManager
        }
        return this
    }
    getRoutes(): DexcomRoutes | undefined {
        return this.routes
    }
    getHandler(): DexcomHandler | undefined {
        return this.handler
    }
    getController(): DexcomController | undefined {
        return this.controller
    }
    getCacheDB(): RedisManager | undefined {
        return this.cacheDataBase
    }
    getCache(): Boolean {
        return this.cache
    }
    getEncryption(): CryptoManagerInterface | undefined {
        return this.encryptionManager
    }
    build(): DexcomDBBuilder {
        return this
    }  
}