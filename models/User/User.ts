
export interface User {
    username: string;
    password: string;
}
export interface UserTokenCredentials {
    userId:string;
    accessToken:string
}