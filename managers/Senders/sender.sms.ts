import { SenderStrategy } from "./sender.interface"

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
// const client = require('twilio')(accountSid, authToken)
import { Twilio } from 'twilio'

export class SmsStrategy implements SenderStrategy {
    twilioClient: any
    statusCallback: string = "https://3bde70325ce6.ngrok.io/api/v1/smsStatusChange"

    constructor() {
        if (!accountSid || !authToken) {
            throw new Error("Twilio credentials are not configured")
        }
        this.twilioClient = new Twilio(accountSid, authToken);
    }

    public setStatusCallback(statusCallback: string) {
        this.statusCallback = statusCallback
    }
 
    public async sendMsg(msg: any): Promise<any> {
        if (!accountSid || !authToken || !msg || !msg.msg || !msg.from || !msg.to) {
            throw new Error("Twilio credentials are not configured")
        }
        if (!msg || !msg.msg || !msg.from || !msg.to) {
            throw new Error("Msg params error")
        }

        try {
            const response = await this.twilioClient.messages
            .create({
                body: msg.msg,
                from: msg.from, // +34976260188
                to: msg.to,
                statusCallback: this.statusCallback
            })
            if (response) {
                return Promise.resolve(response)
            }
            return Promise.reject(new Error("Error sending msg with twilio provider"))
        } catch (error) {
            console.error(error)
            return Promise.reject(error)
        }
    }
}