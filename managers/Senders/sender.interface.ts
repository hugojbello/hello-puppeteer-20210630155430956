import { EmailStrategy } from "./sender.email";
import { SmsStrategy } from "./sender.sms";

export class ContextSenderStrategy {
    private strategy: SenderStrategy;

    constructor(strategy: SenderStrategy) {
        this.strategy = strategy;
    }

    public setStrategy(strategy: SenderStrategy) {
        this.strategy = strategy;
    }

    public async sendMsg(msg: any): Promise<any> {
        try {
            const result = await this.strategy.sendMsg(msg)
            if (result) {
                return Promise.resolve(result)
            }
            return Promise.reject("Something was wrong with serverProvider")
        } catch (error) {
            console.error(error)
            return Promise.reject(error)
        }
    }

    public async setStrategyFromMsgType(msg: any) {
        if (!msg.type) {
            throw new Error("Can not identify the message type.")
        }
        let strategy : SenderStrategy | undefined = undefined
        switch (msg.type) {
            case "sms":
                strategy = new SmsStrategy()
                break;
            case "email":
                strategy = new EmailStrategy()
                break;
            case "push":
                strategy = new EmailStrategy()
                break;
            default:
                strategy = new SmsStrategy()
        }
        this.strategy = strategy
    }
}

export interface SenderStrategy {
    sendMsg(msg: any): Promise<any>;
}