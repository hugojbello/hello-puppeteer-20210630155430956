import { DexcomController } from '../controllers/dexcom.controller';
import { Handler } from '../framework/base/handler.model';
import axios from 'axios'

export class DexcomHandler {
    controller: DexcomController;

    constructor(controller: DexcomController) {
        this.controller = controller
    }

    login: Handler = async (req, res) => {
        try {
            let { username, password } = req.body
            if (!username || !password) {
                return res.status(400).send({ error: 'Bad request' });
            }
            const userCredentials = await this.getLoginData(username, password)

            if (userCredentials) {
                return res.send(userCredentials)
            }
            return res.status(500).send("Something was wrong. Contact with developer@insulclock.com")
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }

    async getLoginData(username: string, password: string) : Promise<any> {
        try {
            const userCredentials = await this.controller.login(username, password)
            if (userCredentials) {
                return Promise.resolve(userCredentials)
            }    
            return Promise.reject(new Error("Can not get userCredentials"))
        } catch (error) {
            console.log(error)
            return Promise.reject(error)
        }   
    }

    data: Handler = async (req, res) => {
        try {
            let { subject, token } = req.body

            if (!subject || !token) {
                return res.status(400).send('Bad request');
            }

            const date = new Date()
            const dateNow = new Date(date.getTime() + 60*60*24*1000)
            const daysBefore = 7
            const dateEnd = req.body.dateEnd ? req.body.dateEnd : dateNow.toISOString().slice(0,10)
            const dateEndDateFormat = new Date(dateEnd)
            const dateStart = req.body.dateStart ? req.body.dateStart : new Date(dateEndDateFormat.getTime() - (daysBefore * 24 * 60 * 60 * 1000)).toISOString().slice(0,10)

            const data = await this.getDataData(subject, token, dateStart, dateEnd)
            if (data) {
                return res.send(data)
            }
            
            return res.status(500).send("Something was wrong. Contact with developer@insulclock.com")
        } catch (error) {
            console.log(error)
            return res.status(400).send(error.toString())
        }
    }

    async getDataData(subject: string, token: string, dateStart: string, dateEnd: string) : Promise<any> {
        try {
            const data = await this.controller.data(subject, token, dateStart, dateEnd)
            if (data) {
                return Promise.resolve(data)
            }
            return Promise.reject(new Error("Can not get data"))
        } catch (error) {
            console.log(error)
            return Promise.reject(error)
        }
        
    }

    loginAndData: Handler = async (req, res, next) => {
        let { username, password } = req.body
        if (!username || !password) {
            console.log("bad request")
            return res.status(400).send({ error: 'Bad request' });
        }

        try {
            const userCredentials = await this.getLoginData(username, password)
            
            if (userCredentials) {                
                const date = new Date()
                const dateNow = new Date(date.getTime() + 60*60*24*1000)
                const daysBefore = 15
                const dateEnd = req.body.dateEnd ? req.body.dateEnd : dateNow.toISOString().slice(0,10)
                const dateEndDateFormat = new Date(dateEnd)
                const dateStart = req.body.dateStart ? req.body.dateStart : new Date(dateEndDateFormat.getTime() - (daysBefore * 24 * 60 * 60 * 1000)).toISOString().slice(0,10)

                const data = await this.getDataData(userCredentials.userId, userCredentials.accessToken, dateStart, dateEnd)
                if (data) {
                    return res.send(data)
                }
            }
            
            return res.status(500).send("Something was wrong. Contact with developer@insulclock.com")
        } catch (error) {
            console.log(error)
            return res.status(500).send(error.toString())
        }
        
    }
}
