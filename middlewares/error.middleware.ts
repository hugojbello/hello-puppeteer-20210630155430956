import HttpException from "../common/http-exception";
import { Request, Response, NextFunction } from "express";
export class ErrorHandler {
    app: any

    constructor(app: any) {
      this.app = app;
    }
  
    // Apply appwise middleware.
    applyMiddlewares() {
      this.app.use(this.logErrors);
      this.app.use(this.xhrErrorHandler);
      this.app.use(this.errorPageHandler);
    }

    // Just print the error and jump to next error middleware.
    logErrors(error: HttpException, request: Request, response: Response, next: NextFunction) {
      console.error(error.stack);
      next(error);
    }

    // If xhr in progress return 500 else jump to next error middleware.
    xhrErrorHandler(error: HttpException, request: Request, response: Response, next: NextFunction) {
      if(request.xhr) {
        response.status(500).send({error: error.stack});
      } else {
        next(error);
      }
    }

    // Render the error page with 500 response code.
    errorPageHandler(error: HttpException, request: Request, response: Response, next: NextFunction) {
        const status = error.statusCode || error.status || 500;
        response.status(status).send(error.stack);
    }
}