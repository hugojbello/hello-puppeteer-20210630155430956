var express = require('express');
var app = express();

const PORT = process.env.PORT || "8080";
app.set("port", PORT);

/*
 * Server
 */
const { createServer } = require('http');
const server = createServer(app);

/*  
 * Init Cache DB. Redis Singleton.
 */
// import { redisManager } from './framework/db/redis.manager';
// redisManager.configureRedisPubSub()

/*
 * Init Mongo DB
 */
// import MongooseManager from './framework/db/mongoose.manager';
// const db = new MongooseManager();
// // Connect and Load models
// import MsgSchema from './models/Msg/msg.mongo.model';
// db.connectMongo().then(() => {
//     new MsgSchema(db.getMongoose())
//     const mongoose = db.getMongoose()
//     console.log("\nMongo models loaded\n======================")
//     console.log(mongoose.models)
// })

/*
 * Config common Middlewares: bodyparser, cors, morgan
 */
import { ConfigCommonMiddleware } from './middlewares/commonMiddleware.config'
const configCommonMiddlewares = new ConfigCommonMiddleware(app) 
configCommonMiddlewares.applyMiddlewares()

/*
 * Error handler middleware
 */
import { ErrorHandler } from './middlewares/error.middleware'
const errorHandler = new ErrorHandler(app);
errorHandler.applyMiddlewares()

/*
 * Auth middleware. Define only if you are going to use it in some Directors or in graphQL
 */
import { JWTManager } from './framework/utils/jwt.manager';
import { AuthMiddleware } from './middlewares/jwt.middleware';
const JWT_TOKEN = process.env.JWT_TOKEN ? process.env.JWT_TOKEN : "Insulcl0ck#69"
if (!JWT_TOKEN) {
    throw new Error("NOT TOKEN DEFINED")
}
const jwtManager = new JWTManager(JWT_TOKEN)
const authMiddleware = new AuthMiddleware(jwtManager)

/*
 * Instance Directors and routes
 */
import { Routes } from './Route'
import { BaseRoute } from './framework/base/route.interface';
const routes: Array<BaseRoute> = []

// Logs Resource
console.log("\nRESOURCES\n======================")
// import { MsgDirector } from './models/Msg/msg.director';
// const msgDirector = new MsgDirector(db, redisManager, authMiddleware)
// msgDirector.buildMongo()
// msgDirector.getRoutes().forEach(route => routes.push(route))
import { DexcomDirector } from './models/Dexcom/dexcom.director';
const dexcomDirector = new DexcomDirector(undefined, undefined, authMiddleware)
dexcomDirector.buildMongo()
dexcomDirector.getRoutes().forEach(route => routes.push(route))

/*
 * Define app namespace and version
 */
const prefix = "/api/v1"

/*
 * Create routes
 */
new Routes(app, prefix, routes);

/*
 * Resource not found. Important define after routes
 */
import { notFoundHandler } from "./middlewares/not-found.middleware";
app.use(notFoundHandler);

/*
 * Start server
 */
server.listen(PORT, () => {
    console.info(`\nServer starting and running on host:${PORT}${prefix}`);
    console.info(`======================`)
});


// app.listen(PORT, () => console.log(`Server running on localhost:${PORT}`));
