import {PuppeteerScraper} from './PuppeteerScraper'

import { User, UserTokenCredentials } from "../models/User/User";

export class LoginScraper extends PuppeteerScraper {
    public timeWaitStart: number
    public timeWaitClick: number
    public userTokenCredentials: UserTokenCredentials ={} as UserTokenCredentials
    public url: string
    //public url = "https://clarity.dexcom.eu/"

    constructor(url: string) {
        super();
        this.timeWaitStart = 1 * 1000
        this.timeWaitClick = 500
        this.url = url
    }

    async scrapTokenAndUserId(user: any): Promise<UserTokenCredentials> {
        
        await this.initializePuppeteer();
        try {
            if (this.page) {

                await this.page.goto(this.url, {waitUntil: 'load', timeout: 0});
                await this.page.waitFor(1 * 1000)
                await this.fillUser(user)
                this.listenApiLoginResponse()

                await this.clickLoginRetrieveUserId()
                await this.closePuppeteer()

                return Promise.resolve(this.userTokenCredentials)
            } else {
                return Promise.reject(new Error("Puppeteer page not exist."))
            }
        } catch (err) {
            console.log(err);
            if (this.page && this.browser) {
                await this.page.screenshot({path: 'error_extract_new.png'});
                await this.browser.close();
            } else {
                return Promise.reject(new Error("Puppeteer page or browser not exist. Scrap"))
            }

            return Promise.reject(new Error("Something was wrong getting token. Scrap"))
        }
    }

    async fillUser(user: User) {
        try {
            if (this.page) {
                await this.page.focus('#username')
                await this.page.keyboard.type(user.username)
                await this.page.focus('#password')
                await this.page.keyboard.type(user.password)
            } else {
                throw new Error("Puppeteer page not exist. Fill User")
            }
        } catch (e) {
            console.log(e)
            throw e
        }
    }

    async clickLoginRetrieveUserId() {
        if (this.page) {
            const element = await this.page.$('input[name="op"]')
            if (element) {
                await element.focus()
                await this.page.keyboard.press('Enter')
                await this.page.waitFor(4 * 1000)

                const userId = await this.page.evaluate(() => {
                    return localStorage.getItem("clarity_currentUser")
                })
                if (userId) {
                    this.userTokenCredentials.userId = userId
                }
            }
        } else {
            throw new Error("Puppeteer page not exist. ClickLoginRetrieveUserId")
        }
    }

    listenApiLoginResponse() {
        if (this.page) {
            this.page.on('request', async (response: any) => {
                const headers = response.headers()
                if (headers["access-token"]){
                    this.userTokenCredentials.accessToken = headers["access-token"]
                }
            });
        } else {
            throw new Error("Puppeteer page not exist. ListenApiLoginResponse")
        }
    }
    async closePuppeteer(){
        try {
            if (this.page && this.browser) {
                await this.browser.close();
                if (!this.page.isClosed()) {
                    await this.page.close();
                }
            } else {
                throw new Error("Puppeteer page or browser not exist. Close Puppeteer")
            }
        } catch (e){
           console.log(e)       
        }
    }
}
