// import { Route } from '../types';
import { Route } from '../framework/base/route.model'
import { BaseRoute, RoutesInterface } from '../framework/base/route.interface';
import { AuthMiddlewareInterface } from '../framework/utils/jwt.middleware.interface';
import { CacheMiddleware } from '../framework/utils/cache.middleware';
import { DexcomHandler } from '../handlers/dexcom.handler';

export class DexcomRoutes {
    handler: DexcomHandler
    routes: BaseRoute[] = []
    authenticationMiddleware: AuthMiddlewareInterface | undefined
    cacheMiddleware?: CacheMiddleware
    
    constructor(handler: DexcomHandler, authenticationMiddleware: AuthMiddlewareInterface | undefined = undefined, cacheMiddleware?: CacheMiddleware) {
        this.handler = handler
        this.authenticationMiddleware = authenticationMiddleware
        this.cacheMiddleware = cacheMiddleware
        this.createRoutes()
    }

    createRoutes() {
        const useMiddlewares : any[] = []
        if (this.authenticationMiddleware) {
           useMiddlewares.push(this.authenticationMiddleware.authenticate)
           useMiddlewares.push(this.authenticationMiddleware.hasRole("admin"))
        }

        this.routes = [
            new Route('post', '/dexcom/login', useMiddlewares, this.handler.login),
            new Route('post', '/dexcom/data', useMiddlewares, this.handler.data),
            new Route('post', '/dexcom/loginAndData', useMiddlewares, this.handler.loginAndData)
        ];
    }
}